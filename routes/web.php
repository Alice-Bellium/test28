<?php

use Illuminate\Support\Facades\Route;


Route::get('/', [\App\Http\Controllers\CarBrandController::class, 'index'])->name('index');
Route::get('/car-models', [\App\Http\Controllers\CarModelController::class, 'index'])->name('car-models');

Route::resource('cars', \App\Http\Controllers\CarController::class);
