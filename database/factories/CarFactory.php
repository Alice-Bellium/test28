<?php

namespace Database\Factories;

use App\Models\CarModel;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Car>
 */
class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'year' => rand(1990, 2023),
            'mileage' => rand(1, 1000),
            'color' => $this->faker->text(3),
            'car_model_id' => CarModel::factory(),
        ];
    }
}
