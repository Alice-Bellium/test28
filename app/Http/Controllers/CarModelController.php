<?php

namespace App\Http\Controllers;

use App\Models\CarModel;

class CarModelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $carModels = CarModel::query()->latest()->paginate(10);
        return view('car-models/index', compact('carModels'));
    }
}
