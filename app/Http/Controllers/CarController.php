<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarRequest;
use App\Models\Car;
use App\Models\CarBrand;
use App\Models\CarModel;
use Illuminate\Support\Arr;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $cars = Car::query()->latest()->paginate(4);
        return view('cars.index', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $carBrands = CarBrand::query()->get();
        $carModels = CarModel::query()->get();

        return view('cars.create', compact(
            'carBrands',
            'carModels',
        ));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CarRequest $request)
    {
        $data = $request->validated();

        /** @var CarBrand $carBrand */
        $carBrand = CarBrand::query()->firstOrCreate([
            'id' => $data['car-brand']
        ]);
        /** @var CarModel $carModel */
        $carModel = $carBrand->carModels()->firstOrCreate([
            'id' => $data['car-model']
        ]);

        $carModel->cars()->create($data);

        $cars = Car::query()->latest()->paginate(4);
        return view('cars.index', compact('cars'))->with('success', 'Автомобиль добавлен');
    }

    /**
     * Display the specified resource.
     */
    public function show(Car $car)
    {
        return view('cars.show', compact('car'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Car $car)
    {
        $carBrands = CarBrand::query()->get();
        $carModels = CarModel::query()->get();

        return view('cars.edit', compact(
            'car',
            'carBrands',
            'carModels',
        ));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CarRequest $request, Car $car)
    {
        $data = $request->validated();

        $car->update(Arr::only($data, [
            'year',
            'mileage',
            'color',
        ]));

        $car->carModel->update([
            'id' => $data['car-model']
        ]);

        $car->carModel->carBrand->update([
            'id' => $data['car-brand']
        ]);

        $carBrands = CarBrand::query()->get();
        $carModels = CarModel::query()->get();

        return view('cars.edit', compact(
            'car',
            'carBrands',
            'carModels',
        ))->with('success', 'Автомобиль обновлен');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Car $car)
    {
        $car->delete();
        $cars = Car::query()->latest()->paginate(4);
        return view('cars.index', compact('cars'))->with('success', 'Автомобиль удален');
    }
}
