<?php

namespace App\Http\Controllers;

use App\Models\CarBrand;

class CarBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $carBrands = CarBrand::query()->latest()->paginate(10);
        return view('index', compact('carBrands'));
    }
}
