<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'year' => ['nullable', 'integer', 'min:1901', 'max:2155'],
            'mileage' => ['nullable', 'integer'],
            'color' => ['nullable', 'string'],
            'car-brand' => ['string', 'exists:car_brands,id'],
            'car-model' => ['string', 'exists:car_models,id'],
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function messages(): array
    {
        return [
            'year.integer' => 'Введите число',
            'year.min:1901' => 'Укажите год больше 1900',
            'year.max:2155' => 'Укажите год меньше 2156',
            'mileage.integer' => 'Введите число',
            'color.string' => 'Введите цвет',
            'car-brand.string' => 'Неверно указана марка',
            'car-brand.exists:car-brands,name' => 'Неверно указана марка',
            'car-model.string' => 'Неверно указана модель',
            'car-model.exists:car-models,name' => 'Неверно указана модель',
        ];
    }
}
