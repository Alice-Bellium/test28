<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * @property int $id Идентификатор записи
 * @property int $year Год выпуска
 * @property int $mileage Пробег
 * @property string $color Цвет
 * @property-read Carbon $created_at Дата создания
 * @property-read Carbon $updated_at Дата обновления
 *
 * @property-read CarModel $carModel
 */
class Car extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'year',
        'mileage',
        'color',
    ];


    /**
     * Get the car model that owns the car.
     */
    public function carModel(): BelongsTo
    {
        return $this->belongsTo(CarModel::class);
    }
}
