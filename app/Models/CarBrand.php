<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * @property int $id Идентификатор записи
 * @property string $name Название
 * @property-read Carbon $created_at Дата создания
 * @property-read Carbon $updated_at Дата обновления
 */
class CarBrand extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];


    /**
     * Get the car models for the car brand.
     */
    public function carModels(): HasMany
    {
        return $this->hasMany(CarModel::class);
    }
}
