<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * @property int $id Идентификатор записи
 * @property string $name Название
 * @property-read Carbon $created_at Дата создания
 * @property-read Carbon $updated_at Дата обновления
 *
 * @property-read CarBrand $carBrand
 */
class CarModel extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];


    /**
     * Get the car brand that owns the car model.
     */
    public function carBrand(): BelongsTo
    {
        return $this->belongsTo(CarBrand::class);
    }


    /**
     * Get the cars for the car model.
     */
    public function cars(): HasMany
    {
        return $this->hasMany(Car::class);
    }
}
