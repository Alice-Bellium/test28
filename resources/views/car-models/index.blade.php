@include('components.header')
<div class="p-4 m-4">
    <div class="body-container">
        <div class="container mb-5">
            <h1 class="display-6">Модели автомобилей</h1>
        </div>
        <div class="container">
            <div class="cars container overflow-hidden">
                @if($carModels->isEmpty())
                    <p class="label-show">Модели автомобилей</p>
                @else
                    @foreach($carModels as $carModel)
                        <div class="card text-center mb-3">
                            <div class="card-header">
                                Модель
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{{ $carModel->name }}</h5>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="d-flex justify-content-center m-3">{!! $carModels->links() !!}</div>
    </div>
</div>
@include('components.footer')
