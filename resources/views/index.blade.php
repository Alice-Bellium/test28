@include('components.header')
<div class="p-4 m-4">
    <div class="body-container">
        <div class="container mb-5">
            <h1 class="display-6">Марки автомобилей</h1>
        </div>
        <div class="container">
            @include('components.messages')
            <div class="cars container overflow-hidden">
                @if($carBrands->isEmpty())
                    <p class="label-show">Марки автомобилей</p>
                @else
                    @foreach($carBrands as $carBrand)
                        <div class="card text-center mb-3">
                            <img src="/{{ $carBrand->name }}.jpg" class="card-img-top img-fluid" alt="...">
                            <div class="card-footer text-muted">
                                <h2 class="card-title">{{ $carBrand->name }}</h2>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="d-flex justify-content-center m-3">{!! $carBrands->links() !!}</div>
    </div>
</div>
@include('components.footer')
