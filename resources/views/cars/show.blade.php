@include('components.header')
<div class="p-4 m-4">
    <div class="body-container">
        <div class="container mb-5">
            <h1 class="display-6">Автомобиль</h1>
        </div>
        <div class="container">
            <form class="col-lg-9">
                <fieldset disabled>
                    <div class="mb-3">
                        <label for="car-brand" class="form-label">Марка автомобиля</label>
                        <input type="text" class="form-control" id="car-brand" name="car-brand"
                               placeholder="Введите модель автомобиля" value="{{ $car->carModel->carBrand->name }}">
                    </div>
                    <div class="mb-3">
                        <label for="car-model" class="form-label">Модель автомобиля</label>
                        <input type="text" class="form-control" id="car-model" name="car-model"
                               placeholder="Введите модель автомобиля" value="{{ $car->carModel->name }}">
                    </div>
                    <div class="mb-3">
                        <label for="year" class="form-label">Год выпуска</label>
                        <input type="number" class="form-control" id="year" name="year"
                               placeholder="Введите год выпуска" value="{{ $car->year }}">
                    </div>
                    <div class="mb-3">
                        <label for="mileage" class="form-label">Пробег</label>
                        <input type="number" class="form-control" id="mileage" name="mileage"
                               placeholder="Введите пробег" value="{{ $car->mileage }}">
                    </div>
                    <div class="mb-3">
                        <label for="color" class="form-label">Цвет автомобиля</label>
                        <input type="text" class="form-control" id="color" name="color"
                               placeholder="Введите цвет автомобиля" value="{{ $car->color }}">
                    </div>
                </fieldset>
            </form>
            <div class="col">
                <div class="">
                    <a class="btn btn-secondary mt-2" role="button"
                       href="{{ route('cars.edit', $car) }}">Редактировать</a>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components.footer')
