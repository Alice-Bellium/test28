@include('components.header')
<div class="p-4 m-4">
    <div class="body-container">
        <div class="m-3">
            <div class="container mb-5">
                <h1 class="display-6">Автомобили</h1>
            </div>
            <div class="container">
                @include('components.messages')
                <div class="cars container overflow-hidden">
                    @if($cars->isEmpty())
                        <p class="label-show">Автомобили</p>
                        <a href="{{ route('cars.create') }}" class="btn btn-primary">Добавить автомобиль</a>
                    @else
                        <a href="{{ route('cars.create') }}" class="btn btn-success mb-5">Добавить автомобиль</a>
                        @foreach($cars as $car)
                            <div class="card text-center mb-3">
                                <div class="card-header">
                                    Автомобиль
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title">{{ $car->carModel->carBrand->name }}</h5>
                                    <h5 class="card-title">{{ $car->carModel->name }}</h5>
                                    <p class="card-text">{{ $car->year }} год</p>
                                    <a href="{{ route('cars.show', $car) }}" class="btn btn-secondary">Подробнее</a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="d-flex justify-content-center m-3">{!! $cars->links() !!}</div>
        </div>
    </div>
</div>
@include('components.footer')
