@include('components.header')
<div class="p-4 m-4">
    <div class="body-container">
        <div class="container mb-5">
            <h1 class="display-6">Редактировать карточку автомобиля</h1>
        </div>
        <div class="container">
            @include('components.messages')
            <form class="col-lg-9" action="{{ route('cars.update', $car) }}" method="post">
                @csrf
                @method('put')
                <div class="mb-3">
                    <label for="car-brand" class="form-label">Марка автомобиля</label>
                    <select class="form-control" aria-label="Введите марку автомобиля" id="car-brand" name="car-brand">
                        @foreach($carBrands as $carBrand)
                            <option @selected($car->CarModel->CarBrand->id === $carBrand->id)
                                    value="{{ $carBrand->id }}">{{ $carBrand->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label for="car-model" class="form-label">Модель автомобиля</label>
                    <select class="form-control" aria-label="Введите модель автомобиля" id="car-model" name="car-model">
                        @foreach($carModels as $carModel)
                            <option @selected($car->CarModel->id === $carModel->id)
                                    value="{{ $carModel->id }}">{{ $carModel->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label for="year" class="form-label">Год выпуска</label>
                    <input type="number" class="form-control" id="year" name="year"
                           placeholder="Введите год выпуска" value="{{ $car->year }}">
                </div>
                <div class="mb-3">
                    <label for="mileage" class="form-label">Пробег</label>
                    <input type="number" class="form-control" id="mileage" name="mileage"
                           placeholder="Введите пробег" value="{{ $car->mileage }}">
                </div>
                <div class="mb-3">
                    <label for="color" class="form-label">Цвет автомобиля</label>
                    <input type="text" class="form-control" id="color" name="color"
                           placeholder="Введите цвет автомобиля" value="{{ $car->color }}">
                </div>
                <div class="mb-3 mt-3">
                    <button class="btn btn-success" role="button" type="submit">Сохранить</button>
                </div>
            </form>
            <div class="col">
                <form action="{{ route('cars.destroy', $car) }}" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-dark" role="button" type="submit">
                        Удалить
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@include('components.footer')
